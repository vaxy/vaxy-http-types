"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
exports.__esModule = true;
var qs_1 = __importDefault(require("qs"));
var http = __importStar(require("http"));
var _ = __importStar(require("lodash"));
var HttpResponse = /** @class */ (function () {
    function HttpResponse(response, data) {
        this.statusCode = response.statusCode;
        this.statusMessage = response.statusMessage;
        this.headers = response.headers;
        if (data) {
            if (this.headers['content-type'] === 'application/json') {
                this.data = JSON.parse(data);
            }
            else {
                this.data = data;
            }
        }
    }
    return HttpResponse;
}());
exports.HttpResponse = HttpResponse;
var HttpClient = /** @class */ (function () {
    function HttpClient(options) {
        this.options = {
            headers: {}
        };
        if (options) {
            this.options = options;
        }
    }
    HttpClient.prototype.request = function (method, entrypoint, query, data, options, attach) {
        var _this = this;
        options = _.merge({}, this.options, options);
        if (data) {
            if (typeof data === 'object') {
                data = JSON.stringify(data);
            }
            options = _.merge(options, {
                headers: {
                    'content-length': Buffer.byteLength(data).valueOf()
                }
            });
        }
        return new Promise(function (resolve, reject) {
            var httpRequest = http.request(Object.assign({
                method: method,
                path: entrypoint + (query ? '?' + qs_1["default"].stringify(query) : ''),
                headers: _this.options.headers
            }, options), function (response) {
                if (attach) {
                    attach(httpRequest, response);
                }
                var data = '';
                response.on('data', function (chunk) {
                    data += chunk.toString();
                });
                response.on('end', function () {
                    var _response = new HttpResponse(response, data);
                    if (_response.statusCode >= 400) {
                        reject(_response);
                    }
                    else {
                        resolve(_response);
                    }
                });
            });
            if (data) {
                httpRequest.write(data);
            }
            if (!attach) {
                httpRequest.end();
            }
        });
    };
    HttpClient.prototype.get = function (entrypoint, query, options, attach) {
        return this.request('GET', entrypoint, query, undefined, options, attach);
    };
    HttpClient.prototype.post = function (entrypoint, query, data, options, attach) {
        return this.request('POST', entrypoint, query, data, options, attach);
    };
    HttpClient.prototype.put = function (entrypoint, query, data, options, attach) {
        return this.request('PUT', entrypoint, query, data, options, attach);
    };
    HttpClient.prototype["delete"] = function (entrypoint, query, data, options, attach) {
        return this.request('DELETE', entrypoint, query, data, options, attach);
    };
    return HttpClient;
}());
exports["default"] = HttpClient;

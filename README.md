# Usage

## Import
``` javascript
const HttpClient = require('vaxy-http-types').default
```

## Basic request
``` javascript
let client = new HttpClient()
client.request('POST', '<http://insert.your/request/path>', /* query */{
  name: "value"
}, /* request body */ "INPUT REQUEST BODY")
```

## More simple API
You can skip `method` parameter of `HttpClient.request` by using `HttpClient.<method>`
``` javascript
let client = new HttpClient()
client.request('POST', '<http://insert.your/request/path>', /* query */{
    name: "value"
}, /* request body */ "INPUT REQUEST BODY").then(response => {
    console.log(response)
}).catch(response => {
    // Throws error if response.statusCode is equal or greater with 400
    // And this "response" is exactly same structure with upper "response" in "then" statement
    console.error(response)
})
```

## Definition
### HttpClient
 - request(method: string, entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions, attach?: (response :http.IncomingMessage) => void): Promise<`HttpResponse`>
 - get (entrypoint: string, query?: object, options?: http.RequestOptions, attach?: (response :http.IncomingMessage) => void): Promise<`HttpResponse`>
 - post (entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions, attach?: (response :http.IncomingMessage) => void): Promise<`HttpResponse`>
 - put (entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions, attach?: (response :http.IncomingMessage) => void): Promise<`HttpResponse`>
 - delete (entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions, attach?: (response :http.IncomingMessage) => void): Promise<`HttpResponse`>

### HttpResponse
 - statusCode: number
 - statusMessage: string
 - headers: object
 - data: string | object

### http.RequestOptions
Nothing special it's just same as [NodeJS http.request options](https://nodejs.org/api/http.html#http_http_request_url_options_callback)

### What is attach?
Every request is return `Promise` and this `Promise` will be resolve at the end of request.
so, if you wanna control response deeper you can use obtain `http.IncomingMessage` object by using attach function.
(You must call `http.IncomingMessage.close` manually for terminate request)
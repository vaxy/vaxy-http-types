import qs from 'qs'
import * as http from 'http'
import * as _ from 'lodash'


export type RequestAttach = (request: http.ClientRequest, response: http.IncomingMessage) => void

export class HttpResponse {
  statusCode: number
  statusMessage: string
  headers: object
  data: string | object

  constructor (response: http.IncomingMessage, data?: string) {
    this.statusCode = response.statusCode
    this.statusMessage = response.statusMessage
    this.headers = response.headers
    if (data) {
      if (this.headers['content-type'] === 'application/json') {
        this.data = JSON.parse(data)
      } else {
        this.data = data
      }
    }
  }
}

export default class HttpClient {

  options: http.RequestOptions = {
    headers: {}
  }

  constructor (options?: http.RequestOptions) {
    if (options) {
      this.options = options
    }
  }

  request (method: string, entrypoint: string, query?: object,
      data?: string | object, options?: http.RequestOptions,
      attach?: RequestAttach): Promise<HttpResponse> {
    options = _.merge({}, this.options, options)
    if (data) {
      if (typeof data === 'object') {
        data = JSON.stringify(data)
      }
      options = _.merge(options, {
        headers: {
          'content-length': Buffer.byteLength(data).valueOf()
        }
      })
    }

    return new Promise((resolve, reject) => {
      var httpRequest = http.request(Object.assign({
        method: method,
        path: entrypoint + (query ? '?' + qs.stringify(query) : ''),
        headers: this.options.headers
      }, options), (response: http.IncomingMessage) => {
        if (attach) {
          attach(httpRequest, response)
        }
        let data = ''
        response.on('data', chunk => {
          data += chunk.toString()
        })
        response.on('end', () => {
          let _response = new HttpResponse(response, data)
          if (_response.statusCode >= 400) {
            reject(_response)
          } else {
            resolve(_response)
          }
        })
      })
      if (data) {
        httpRequest.write(data)
      }
      if (!attach) {
        httpRequest.end()
      }
    })
  }

  get (entrypoint: string, query?: object, options?: http.RequestOptions,
      attach?: RequestAttach): Promise<HttpResponse> {
    return this.request('GET', entrypoint, query, undefined, options, attach)
  }

  post (entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions,
      attach?: RequestAttach): Promise<HttpResponse> {
    return this.request('POST', entrypoint, query, data, options, attach)
  }

  put (entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions,
      attach?: RequestAttach): Promise<HttpResponse> {
    return this.request('PUT', entrypoint, query, data, options, attach)
  }

  delete (entrypoint: string, query?: object, data?: string | object, options?: http.RequestOptions,
      attach?: RequestAttach): Promise<HttpResponse> {
    return this.request('DELETE', entrypoint, query, data, options, attach)
  }

}